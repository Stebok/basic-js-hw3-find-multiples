// let number;
// while (!Number.isInteger(number)) {number = +prompt("Please type a integer number", "");}

let number;
do {
  number = +prompt("Please type a integer number");
} while (number % 1 !== 0);

if (number <= 4 ) {
    console.log('Sorry, no numbers');
}

for (let i = 1; i <= number; i++) {
   if (i % 5 == 0 ) {
   console.log(i);   
}    
}

let m;
do {
   m = +prompt("Please type a number prime m");
} while (m % 1 !== 0 || m == 1);

let n;
do {
   n = +prompt("Please type a number prime n that should not be less than the number prime m");
} while (n % 1 !== 0 || n == 1 || n < m);

for (let i = m; i <= n; i++) {
   let isPrime = true;
   for (let j = 2; j < i; j++) {
      if (i % j === 0) { isPrime = false; break; }
  }
  if (isPrime) console.log(i);
}